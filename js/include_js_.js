$(function() { 
    includeLayout(); 
    console.log(readTextFile("/smain/all_menu_inc.html"));    

});



function includeLayout() { 
    var includeArea = $('[data-include]'); var self, url;
    $.each(includeArea, function() { 
        self = $(this);
        url = self.data("include");
        self.load(url, function() { 
            self.removeAttr("data-include"); 
            active_menu();
        }); 
    }); 

}

function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                console.log("alltext: " + allText);
                // fileDisplayArea.innerText = allText 
            }
        }
    }
    rawFile.send(null);
}


function getContentHtml(title){
    if(title == "개요"){
       return `
        <div id="locationWrap">
        <ul>
            <li><img class="location-home" src="../images/smain/ic-home.svg"></li>
            <li><img class="location-arrow" src="../images/smain/ic-keyboard-arrow-right.svg"></li>
            <li id="location-com">
                <p class="P2Gray2Left-regular">가맹점</p>
            </li>
            <li><img class="location-arrow" src="../images/smain/ic-keyboard-arrow-right.svg"></li>
            <li>
                <p class="P2Gray2Left-regular">개요</p>
            </li>
        </ul>
    </div>

    <div id="conMtitleWrap">
        <ul>
            <li><p class="H1BlackCenter-Medium">개요</p></li>
        </ul>
    </div>

    <div id="conTitleImg" class="conTitleImg01">
        <div id="conTitleImgTxt">
            <ul>
                <li><p class="H4WhiteLeft-bold">TJ 미디어 노래방 반주기 카드 결제 시스템</p></li>
                <li><p class="H4WhiteLeft-light">제품 소개입니다.</p></li>
            </ul>
        </div>
    </div>

    <div id="conWrap">        
        <div id="contents">
            <ul>
                <li class="H5BlackLeft-bold txt_ic01">개요</li>
            </ul>
            <ul class="margin-b24">
                <li class="H6BlackLeft-medium txt_ic02">결제 시스템이란 : TJ미디어 반주기에서 다양한 결제 시스템을 지원합니다.</li>
            </ul>
            <div id="affContentsTxtBox010101">
                <ul>
                    <li><img src="../images/smain/smain01/ic-credit-card.svg"></li>
                    <li class="H1BlueGreenLeft-bold">카드결제</li>
                    <li class="P1BlackCenter-regular">BC카드, 국민카드, 롯데카드 등 <br>8개 카드사 지원</li>
                </ul>
                <ul>
                    <li><img src="../images/smain/smain01/ic-qrcode.svg"></li>
                    <li class="H1BlueGreenLeft-bold">QR결제</li>
                    <li class="P1BlackCenter-regular">BC카드 QR 결제를 지원합니다. <br>(추후 다양한 QR 결제를 지원 예정)</li>
                </ul>
                <ul>
                    <li><img src="../images/smain/smain01/ic-gift.svg"></li>
                    <li class="H1BlueGreenLeft-bold">Point 결제</li>
                    <li class="P1BlackCenter-regular">카드 결제에 대한  <br>포인트 적립 및 포인트 사용을 지원합니다. <br>(현재 개발 진행중)</li>
                </ul>
            </div>
            <ul class="margin-b16">
                <li class="H6BlackLeft-medium txt_ic02">지원 모델 : 60 시리즈부터 지원합니다.</li>
            </ul>
            <ul class="margin-b16">
                <li class="H6BlackLeft-medium txt_ic02">결제 시스템 이용을 위한 필요 장비</li>
                <li class="P1BlackLeft-regular txt_ic03">TCR-1 (Tjmedia Credit Card Reader) 설치 필수</li>
            </ul>
            <ul class="margin-b120">
                <li class="H6BlackLeft-medium txt_ic02">왜 결제버튼이 필요한가?</li>
                <li class="P1BlackLeft-regular txt_ic03">코인은 돈을 투입하는 즉시 반주기에 코인/시간이 추가 됩니다.</li>
                <li class="P1BlackLeft-regular txt_ic03">하지만 결제리더기를 이용할 경우 언제, 어떻게 동작 해야 하는지 명확한 가이드가 필요 합니다.</li>
                <li class="P1BlackLeft-regular txt_ic03">따라서 소비자가 쉽게 이용할 수 있도록 결제 버튼을 별도로 제공한 것 입니다.</li>
            </ul>
            <ul class="margin-b56">
                <li class="H5BlackLeft-bold txt_ic01">결제 Flow</li>
            </ul>
            <img class="margin-b120" src="../images/smain/smain01/affContentsImg010101.png">
            <ul class="margin-b56">
                <li class="H5BlackLeft-bold txt_ic01">BC MPM 방식</li>
            </ul>
            <img src="../images/smain/smain01/affContentsImg010102.png">       
        </div>
    </div>
    
    <footer role="footer" data-include="footer_inc.html"></footer>
    <script type="text/javascript" src="../js/include_js.js"></script>   
        `;
    }
}
